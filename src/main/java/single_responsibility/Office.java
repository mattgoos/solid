package single_responsibility;

public class Office {
  public static void main(String[] args) {
    Employee e = new Employee("Matthias", 1000);

    e.saySomething();
    e.sayYourWager();
    e.describe();
    e.save();
  }
}
