package single_responsibility;

import java.time.LocalDate;
import java.time.Period;

public class Employee {

  String name;
  Integer currentWager;
  LocalDate workingSince;

  public Employee(String name, Integer wager) {
    this.name = name;
    this.currentWager = wager;
    this.workingSince = LocalDate.now();
  }

  public Employee(String name, Integer currentWager, LocalDate workingSince) {
    this(name, currentWager);
    this.workingSince = workingSince;
  }

  public double calculateWager() {
    System.out.println("Calculating wager for employee " + this.name);

    return currentWager + 0.5 * this.getBonus();
  }

  double getBonus() {
    return Period.between(LocalDate.now(), workingSince).getDays();
  }

  String saySomething() {
    return "Hello :)";
  }

  String sayYourWager() {
    return "I'm earning " + this.calculateWager();
  }

  String describe() {
    return this.name + " has been working here since " + this.workingSince +
       "and earns " + this.calculateWager() + "each month";
  }

  void save() {
    System.out.println("Saving employee to the imaginary DB");
  }
}
