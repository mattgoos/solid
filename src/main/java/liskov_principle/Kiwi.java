package liskov_principle;

public class Kiwi extends Bird {

    String currentLocation;

    @Override
    public void fly() {
        throw new UnsupportedOperationException();
    }

    public void swim() {
        this.currentLocation = "in the water";
    }
}
