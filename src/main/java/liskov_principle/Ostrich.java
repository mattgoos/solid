package liskov_principle;

public class Ostrich extends Bird {
  @Override
  public void fly() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void molt() {
    System.out.println("Ostrich is losing his feathers");
  }
}
