package liskov_principle;

public class Bird {
    public void fly() {
        System.out.println("Flying bird");
    }

    public void molt() {
        System.out.println("bird loses a feather");
    }

    public void eat() {
        System.out.println("The bird is eating");
    }
}
