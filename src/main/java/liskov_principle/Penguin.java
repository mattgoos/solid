package liskov_principle;

public class Penguin extends Bird {

    @Override
    public void fly() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void molt() {
        super.molt();
    }
}
