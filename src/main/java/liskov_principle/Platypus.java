package liskov_principle;

public class Platypus extends Kiwi {
  @Override
  public void swim() {
    System.out.println("Platypus in the water");
  }
}
