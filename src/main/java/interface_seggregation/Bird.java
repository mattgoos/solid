package interface_seggregation;

public interface Bird {
    public void fly();

    public void molt();
}
