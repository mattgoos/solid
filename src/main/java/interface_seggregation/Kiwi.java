package interface_seggregation;

public class Kiwi {
    String currentLocation;
    int numberOfFeathers;

    public Kiwi(int initialFeatherCount) {
        this.numberOfFeathers = initialFeatherCount;
    }

    public void shed() {
        this.numberOfFeathers -= 2;
    }

    public void takeFlight() {
        throw new UnsupportedOperationException();
    }

    public void swim() {
        this.currentLocation = "in the water";
    }
}
